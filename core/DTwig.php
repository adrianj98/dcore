<?php
/**
 *
 */

//DCore::using('twig:Autoloader');
Twig_Autoloader::register();

class DTwig_Loader_Filesystem extends Twig_Loader_Filesystem {

    public function findTemplate($name) {
        if (is_file($name))
            return $this->cache[$name] = $name;
        $path = DCore::getFilePath($name, 'views', 'default', '.twig', false);
        if ($path)
            return $path;

        return parent::findTemplate($name);
    }
}

class DTwig extends baseClass {

    static $loader = null;
    static $extensions;
    private $twig = null;

    function render($file, $templateClass, $vars = array()) {
        if (!is_array($vars))
            $vars = array();
        $templateVars = $this->registry->template->getVars();
        $vars = $templateVars + $vars;
        $template = $this->twig->loadTemplate($file);
        $result   = $template->display($vars);

        return $result;
    }

    function init() {
        $loader = new DTwig_Loader_Filesystem(DCore::getPathOfAlias('app'));
        global $registry;

        $this->twig = new Twig_Environment($loader, array(
            'cache' => false,// DCore::getPathOfAlias('runtime'),
            'autoescape' => false,
        ));

        $this->twig->addGlobal('template', $registry->template);
        $this->twig->addGlobal('registry', $registry);
        foreach(self::$extensions as $ext){
            $this->twig->addExtension(new $ext());
        }
      //  $function = new Twig_SimpleFunction('render', 'DTwig_render', array('needs_environment' => true, 'needs_context' => true));
       // $this->twig->addFunction($function);

    }
}

function DTwig_render($enviroment, $context = array(), $templateName = null, $vars = false) {

    global $registry;

    return $registry->template->render($templateName, $vars);

}

class DCore_registry_TokenParser extends Twig_TokenParser
{
    public function parse(Twig_Token $token)
    {
        $parser = $this->parser;
        $stream = $this->parser->getStream();


          //  $stream->next();
            $code = $this->parser->getExpressionParser()->parseMultitargetExpression();

            $stream->expect(Twig_Token::BLOCK_END_TYPE);

        return new DCore_registry_Node($code,  $token->getLine(), $this->getTag());
    }

    public function getTag()
    {
        return 'registry';
    }
}

class DCore_registry_Node extends Twig_Node
{
    public function __construct($code,  $line, $tag = null)
    {
        parent::__construct( array('code' => $code), array(),$line, $tag);
    }

    public function compile(Twig_Compiler $compiler)
    {

        $compiler
            ->addDebugInfo($this)

            ->subcompile($this->getNode('code'))
            ->raw(";\n")
        ;


    }
}

class DCore_registry_Extension extends Twig_Extension
{
    public function getName()
    {
        return 'DCore';
    }
    public function getTokenParsers()
    {
        return array(new DCore_registry_TokenParser());
    }


}

DTwig::$extensions["DCore_registry_Extension"] = "DCore_registry_Extension";